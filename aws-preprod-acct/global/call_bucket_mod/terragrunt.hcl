locals {
    account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl", "${path_relative_from_include()}/account.hcl")) 
    region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl")) 
    environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
    target_account = local.account_vars.locals.aws_account_id
}

terraform {
    source = "git::https://fmbah_bb@bitbucket.org/cs-104/dm-tf-modules.git//tf-s3-module?ref=master"
}

include {
    path = find_in_parent_folders()
}

inputs = {
    bucket_name        = "deployer-servicaccount1-189876354"
    access_level       = "private"
}