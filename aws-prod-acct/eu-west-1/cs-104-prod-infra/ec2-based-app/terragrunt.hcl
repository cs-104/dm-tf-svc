locals {
    account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl", "${path_relative_from_include()}/account.hcl")) 
    region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl")) 
    environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
    target_account = local.account_vars.locals.aws_account_id
}

terraform {
    source = "git::https://michaeloshaye@bitbucket.org/cs-104/cs-104-michael.git//lesson10d/dm-tf-modules/tf-ec2-module?ref=master"
}

include {
    path = find_in_parent_folders()
}

inputs = {
    aws_ami = "ami-0b850cf02cc00fdc8"
    server_type = "t2.micro"
    target_keypairs = "prod-paris-keys"
    acct_vpc_id = "vpc-e62b9d9f"
    acct_subnet_id = "subnet-5682701d"
    env_name = "production"
}
