locals {
    account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl", "${path_relative_from_include()}/account.hcl")) 
    region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl")) 
    environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
    target_account = local.account_vars.locals.aws_account_id
}

terraform {
    source = "git::https://fmbah_bb@bitbucket.org/cs-104/dm-tf-modules.git//tf-vpc-module?ref=master"
}

include {
    path = find_in_parent_folders()
}

inputs = {
    ip_range          = "10.104.0.0/16"
    inst_tenancy      = "default"
    dns_support       = true
    dns_hostn         = true
    which_env_nam_tag = "dev_env"

    pub_ip_range = ["10.104.1.0/24", "10.104.2.0/24", "10.104.5.0/24"]
    pub_azs      = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]


    priv_ip_range = ["10.104.3.0/24", "10.104.4.0/24"]
    priv_azs      = ["eu-west-1a", "eu-west-1b"]
}