locals {
    account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl", "${path_relative_from_include()}/account.hcl")) 
    region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl")) 
    environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
    target_account = local.account_vars.locals.aws_account_id
}

terraform {
    source = "git::https://fmbah_bb@bitbucket.org/cs-104/dm-tf-modules.git//tf-ec2-module?ref=master"
}

include {
    path = find_in_parent_folders()
}

inputs = {
    aws_ami = "ami-0b850cf02cc00fdc8"
    server_type = "t2.micro"
    target_keypairs = "admin_sandbox_kp"
    acct_vpc_id = "vpc-047dc1c4077ef720b"
    acct_subnet_id = "subnet-0cc1a2b959ad58962"
    env_name = "sandbox"
}